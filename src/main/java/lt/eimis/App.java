package lt.eimis;

import lt.eimis.entities.CowHierarchy;
import lt.eimis.exception.AlreadyExistingCowException;
import lt.eimis.exception.ImmortalCowException;
import lt.eimis.exception.NonExistentCowException;

import static lt.eimis.entities.CowHierarchy.*;

public class App
{

    public static void main(String[] args ) throws NonExistentCowException, AlreadyExistingCowException, ImmortalCowException {

        CowHierarchy cowHierarchy = new CowHierarchy();
        cowHierarchy.GiveBirth(FIRST_COW_ID, 2, "cow 2");
        cowHierarchy.GiveBirth(2, 3, "cow 3");
        cowHierarchy.GiveBirth(2, 4, "cow 4");
        cowHierarchy.GiveBirth(3, 10, "cow 10");
        cowHierarchy.EndLifeSpan(3);
        cowHierarchy.PrintFarmData();
    }


}
