package lt.eimis.exception;

public class ImmortalCowException extends Exception {
    public ImmortalCowException() {
        super("Cannot delete the immortal cow");
    }
}
