package lt.eimis.exception;

public class AlreadyExistingCowException extends Exception {

    public AlreadyExistingCowException(String s) {
        super(s);
    }
}
