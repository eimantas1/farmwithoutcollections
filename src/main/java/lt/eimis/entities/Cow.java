package lt.eimis.entities;

import lombok.Data;

@Data
public class Cow {
    private long cowId;
    private String nickName;
    private boolean deleted;

    public Cow(long cowId, String nickName) {
        this.cowId = cowId;
        this.nickName = nickName;
    }
}
