package lt.eimis.entities;

public class CowList {

    CowNode firstCowNode;

    static class CowNode {
        final Cow cow;
        CowNode next;
        CowNode(Cow c)
        {
            cow = c;
            next = null;
        }
    }

    public void insert(Cow data)
    {
        CowNode newCowNode = new CowNode(data);
        newCowNode.next = null;

        if (firstCowNode == null) {
            firstCowNode = newCowNode;
        }
        else {
            CowNode last = firstCowNode;
            while (last.next != null) {
                last = last.next;
            }
            last.next = newCowNode;
        }
    }

    public void softDeleteByCowId(long cowId)
    {
        CowNode currCowNode = firstCowNode;
        while (currCowNode != null) {
            if(currCowNode.cow.getCowId() == cowId){
                currCowNode.cow.setDeleted(true);
                return;
            }
            currCowNode = currCowNode.next;
        }
    }

    public void print()
    {
        CowNode currCowNode = firstCowNode;
        System.out.println("Old McDonald had a farm: ");
        while (currCowNode != null) {
            if(!currCowNode.cow.isDeleted()) {
                System.out.println(currCowNode.cow + " ");
            }
            currCowNode = currCowNode.next;
        }
        System.out.println("E-I-E-I-O");
    }

    public boolean contains(long cowId) {
        CowNode currCowNode = firstCowNode;
        while (currCowNode != null) {
            if(currCowNode.cow.getCowId() == cowId && !currCowNode.cow.isDeleted()){
                return  true;
            }
            currCowNode = currCowNode.next;
        }
        return false;
    }

    public int getCowCount() {
        int i = 0;
        CowNode currCowNode = firstCowNode;
        while (currCowNode != null) {
            if(!currCowNode.cow.isDeleted()) {
                i++;
            }
            currCowNode = currCowNode.next;
        }
        return i;
    }
}
