package lt.eimis.entities;

import lt.eimis.exception.AlreadyExistingCowException;
import lt.eimis.exception.ImmortalCowException;
import lt.eimis.exception.NonExistentCowException;

public class CowHierarchy {
    public static final long FIRST_COW_ID = 0;
    public static final String FIRST_COW = "The First Cow";

    private final CowList listOfCows = new CowList();

    public CowHierarchy() {
        listOfCows.insert(new Cow(FIRST_COW_ID, FIRST_COW));
    }

    public void GiveBirth(long parentCowId, long cowId, String name) throws AlreadyExistingCowException, NonExistentCowException {
        if (!cowExists(parentCowId)) {
            throw new NonExistentCowException("Cannot find parent cow with id " + parentCowId);
        }
        if (cowExists(cowId)) {
            throw new AlreadyExistingCowException("Cannot add cow with id " + cowId + " because it does not exits.");
        }
        listOfCows.insert(new Cow(cowId, name));
    }

    public void EndLifeSpan(long cowId) throws NonExistentCowException, ImmortalCowException {
        if (cowId == FIRST_COW_ID) {
            throw new ImmortalCowException();
        }

        if (!cowExists(cowId)) {
            throw new NonExistentCowException("Cannot delete cow with id " + cowId + " because it does not exits.");
        }
        listOfCows.softDeleteByCowId(cowId);
    }

    public void PrintFarmData() {
        listOfCows.print();
    }

    private boolean cowExists(long cowId) {
        return listOfCows.contains(cowId);
    }

    public int getCowCount() {
        return  listOfCows.getCowCount();
    }
}
